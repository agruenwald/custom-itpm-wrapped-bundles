CDL FLEX Wrapped Jena Libraries
================================

Wrapped Jena libraries. The libraries have been moved from hp to the
Apache Jena project. The earlier TDB library does not support
transactions, hence the new sources are necessary. 

The OWL API and the Pellet Reasoner still use the old sources, hence
these projects needed to be split.